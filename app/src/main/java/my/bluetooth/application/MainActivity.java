package my.bluetooth.application;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Set;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button on_Button, visible_Button, list_Button, off_Button;
    private ListView listView;

    private BluetoothAdapter bAdapter;

    private Set<BluetoothDevice> pairedDevices;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bindViews();

    }

    private void bindViews() {

        bAdapter = BluetoothAdapter.getDefaultAdapter();

        on_Button = findViewById(R.id.on_Button);
        visible_Button = findViewById(R.id.visible_Button);
        list_Button = findViewById(R.id.list_Button);
        off_Button = findViewById(R.id.off_Button);
        listView = findViewById(R.id.listView);

        on_Button.setOnClickListener(this);
        visible_Button.setOnClickListener(this);
        list_Button.setOnClickListener(this);
        off_Button.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.on_Button:
                onDevice();
                break;

            case R.id.visible_Button:
                Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
                startActivityForResult(intent, 0);
                break;

            case R.id.list_Button:
                showDevices();
                break;

            case R.id.off_Button:
                bAdapter.disable();
                Toast.makeText(this, "Turned Off", Toast.LENGTH_SHORT).show();
                break;

        }
    }

    private void showDevices() {

        pairedDevices = bAdapter.getBondedDevices();
        ArrayList listA = new ArrayList();

        for (BluetoothDevice bd : pairedDevices) {
            listA.add(bd.getName());
        }

        final ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, listA);
        listView.setAdapter(adapter);

    }

    private void onDevice() {
        if (!bAdapter.isEnabled()) {
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(intent, 0);

            Toast.makeText(this, "Turned ON", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Already ON", Toast.LENGTH_SHORT).show();
        }
    }

}
